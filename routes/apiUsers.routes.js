let Model = require('../models/user.model');
var passwordHash = require('password-hash');

var Mailgun = require('mailgun').Mailgun;

exports.registerForm = function(req, res) {
    res.render('admin/register');
};
exports.register = function(req, res, next) {

    var user = new Model;
    user.username = req.body.username;
    user.password = req.body.password;
    user.name = req.body.name;

    user.save(function(err, savedUser) {
        if (err) {next(err)};

        console.log('savedUser', savedUser);
        return {message: 'Salvo com sucesso', data: savedUser}
    });

};


exports.index = async function(req, res) {

    let all = await Model.find({}).sort({nome: 1});

    return res.send({message: 'Lista de usuarios', data: all});
};

exports.create = async function(req, res) {
    let one = new Model;

    if (req.query.tipo) {
        one.tipo = req.query.tipo;
    }

    // let locais = await Local.find({tipo: "UNIDADE"}).sort('nome');

    return res.render('usuarios/form', {mode: 'create', one: one});
};


exports.store = async function(req, res, next) {

    try {
        let data = req.body;

        data.username = data.username.toLowerCase(); //fix

        if (data.locais == "") {
            data.locais = [];
        }

        console.log("data", data);

        data.password = Math.random().toString(36).substring(7);

        let user = new Model(data);

        await user.save();

        return res.send({message: 'Cadastro concluído com sucesso', data: user});

        // req.flash('success', 'Cadastro concluído com sucesso');
        //
        // res.redirect('/usuarios');
    } catch(e) {
        next(e);
    }



};

var passwordHash = require('password-hash');

exports.reset_pass_via_admin = async function(req, res, next) {

    try {

        const user = await Model.findOne({_id: req.params.id});

        const pass = Math.random().toString(36).substring(7);
        const hashed_pass = passwordHash.generate(pass);

        const data = {password: hashed_pass};

        await Model.findOneAndUpdate({_id: req.params.id}, {$set: data}, { runValidators: true, context: 'query' });

        req.flash('success', 'Nova senha foi disparada com sucesso!');

        //enviar email

        res.redirect('/usuarios');
    } catch(e) {
        next(e);
    }

};

exports.getById = async function(req, res, next) {

    let id = req.params.id;

    let one = await Model.findOne({_id: id});

    // let locais = await Local.find({tipo: "UNIDADE"}).sort('nome');

    return res.send({message: 'Usuario', data: one})

    // return res.render("usuarios/form", {mode : "edit", one: one});

};

exports.update = async function(req, res, next) {

    try {
        let id = req.params.id;

        let data = req.body;

        const old_user = await Model.findOne({username: req.body.username});

        if (old_user && old_user._id.toString() == id) {
            delete data.username;
        } else {
            data.username = data.username.toLowerCase(); //fix
        }

        console.log("data", data);

        if (data.locais == "" || !data.locais) {
            data.locais = [];
        }

        if (req.body.new_password) {
            data.password = passwordHash.generate(req.body.new_password);
        }

        await Model.findOneAndUpdate({_id: id}, {$set: data}, { runValidators: true, context: 'query' });

        return res.send({message: 'Registro atualizado com sucesso', data: ''});

        // req.flash('success', 'Registro atualizado com sucesso');
        // return res.redirect('/usuarios');
    } catch(e) {
        next(e);
    }



};

exports.ativar = async function(req, res, next) {

    try {
        let id = req.params.id;

        const data = {
            status: 1
        };

        await Model.findOneAndUpdate({_id: id}, {$set: data}, { runValidators: true, context: 'query' });

        req.flash('success', 'Registro atualizado com sucesso');
        return res.redirect('/usuarios');
    } catch(e) {
        next(e);
    }

};

exports.desativar = async function(req, res, next) {

    try {
        let id = req.params.id;

        const data = {
            status: 2
        };

        await Model.findOneAndUpdate({_id: id}, {$set: data}, { runValidators: true, context: 'query' });

        req.flash('success', 'Registro atualizado com sucesso');
        return res.redirect('/usuarios');
    } catch(e) {
        next(e);
    }

};


exports.delete = async function(req, res, next) {

    let id = req.params.id;

    await Model.findOneAndRemove({_id: id});

    return res.send({message: 'Registro removido com sucesso', data: ''});

    // req.flash('success', 'Registro removido com sucesso!');
    // return res.redirect('/usuarios')

};
