var requireGuest = require('../lib/require-guest');
var requireLogin = require('../lib/require-login');

// Controllers
var admin = require('./admin.routes');
var user = require('./users.routes');
var apiUser = require('./apiUsers.routes');
var sessions = require('./sessions.routes');


var permit = require('../domain/permission');


const maintenance_mode = process.env.MAINTENANCE_MODE || "NO";

module.exports = function(app) {

    app.use(function(req, res, next) {

        if (maintenance_mode == "YES") {
            return res.render('manutencao');
        }

        next();

    });

    app.get('/usuarios', permit("ADMIN"), requireLogin, user.index);
    app.get('/usuarios/create', permit("ADMIN"), requireLogin, user.create);
    app.post('/usuarios', permit("ADMIN"), requireLogin, user.store);
    app.get('/usuarios/:id/edit', permit("ADMIN"), requireLogin, user.edit);
    app.get('/usuarios/:id/desativar', permit("ADMIN"), requireLogin, user.desativar);
    app.get('/usuarios/:id/ativar', permit("ADMIN"), requireLogin, user.ativar);
    app.post('/usuarios/:id', permit("ADMIN"), requireLogin, user.update);
    app.get('/usuarios/:id/delete', permit("ADMIN"), requireLogin, user.delete);
    // app.get('/usuarios/:id/reset-pass', permit("ADMIN"), requireLogin, user.reset_pass_via_admin);

    app.get('/api/usuarios', apiUser.index);
    // app.get('/api/usuarios/create', apiUser.create);
    app.post('/api/usuarios', apiUser.store);
    app.post('/api/usuarios/:id', apiUser.update);
    app.get('/api/usuarios/:id', apiUser.getById);
    app.get('/api/usuarios/:id/delete', apiUser.delete);

    // Admin
    app.get('/', requireLogin, user.index);

    //====================

    // Users
    app.get('/register', requireGuest, user.registerForm);
    app.post('/register', user.register);

    // Sessions
    app.get('/login', requireGuest, sessions.loginForm);
    app.post('/login', sessions.login);
    app.post('/change-password', sessions.new_pass);
    app.get('/logout', sessions.logout);


};
