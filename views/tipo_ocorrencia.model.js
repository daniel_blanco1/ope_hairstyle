var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    nome: String,
}, {collection: "tipos_ocorrencia"});

var Model = mongoose.model('TipoOcorrencia', schema);

module.exports = Model;