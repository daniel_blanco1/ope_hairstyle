var fs = require('graceful-fs');
var inlineCss = require('inline-css');

var my_app;
var key;
var from_email;
var from_name;
var domain;

exports.initialize = function(app, options) {
    my_app = app;
    apiKey = options.apiKey || "";
    publicApiKey = options.publicApiKey || "";
    from_email = options.from_email || "";
    from_name = options.from_name || "";
    domain = options.domain || 'dc.blanco02@gmail.com';

    return function(req, res, next) {
        next();
    }
};

exports.sendByView = function(to, subject, template_name, data, callback, _from_email, _from_name) {

    to = to.toLowerCase();

    my_app.render(template_name, data, function(err, body) {

        if(err) {
            return callback(err);
        }

        var remetente = _from_email ? _from_email : from_email;
        var de = _from_name ? _from_name : from_name;

        var mailgun = require('mailgun-js')({apiKey: apiKey, publicApiKey: publicApiKey, domain: domain});

        var data = {
            from: de+' <'+remetente+'>',
            to: to,
            subject: subject,
            html: body,
        };

        mailgun.messages().send(data, function (error, body) {
            console.log("error");
            console.log(error);
            callback(error, body);
        });

    });

};

exports.sendByViewWithCC = function(to, subject, template_name, data, callback, cc) {

    to = to.toLowerCase();

    my_app.render(template_name, data, function(err, body) {

        if(err) {
            return callback(err);
        }

        console.log("cc", cc)

        var remetente = from_email;
        var de = from_name;

        var mailgun = require('mailgun-js')({apiKey: apiKey, publicApiKey: publicApiKey, domain: domain});

        var data = {
            from: de+' <'+remetente+'>',
            to: to,
            cc: cc,
            subject: subject,
            html: body,
        };

        mailgun.messages().send(data, function (error, body) {
            console.log("error");
            console.log(error);
            callback(error, body);
        });

    });

};

exports.sendByViewAndStripCss = function(to, subject, template_name, data, callback, _from_email, _from_name, other_data) {

    my_app.render(template_name, data, function(err, body) {

        if(err) {
            return callback(err);
        }

        var remetente = _from_email ? _from_email : from_email;
        var de = _from_name ? _from_name : from_name;

        var mailgun = require('mailgun-js')({apiKey: apiKey, publicApiKey: publicApiKey, domain: domain});

        inlineCss(body, {url: "/"})
            .then(function(html) {

                var data = {
                    from: de+' <'+remetente+'>',
                    to: to,
                    subject: subject,
                    html: html
                };

                if (other_data) {
                    data['v:my-custom-data'] = other_data;
                }

                mailgun.messages().send(data, function (error, body) {
                    console.log("error");
                    console.log(error);
                    callback(error, body);
                });

            });



    });

};
