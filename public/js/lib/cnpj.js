/*!
*   Gerador e Validador de CNPJ
*/

'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/**
 * CNPJ Class
 *
 * validate function
 * @param  {string} value      The value for validation
 * @return {boolean}           True = valid || False = invalid
 *
 * @return {string}            Formatted CNPJ || error message
 */

(function () {
    var root = (typeof self === 'undefined' ? 'undefined' : _typeof(self)) === 'object' && self.self === self && self || (typeof global === 'undefined' ? 'undefined' : _typeof(global)) === 'object' && global.global === global && global || this;

    var CNPJ = function CNPJ() {};

    if (typeof exports !== 'undefined' && !exports.nodeType) {
        if (typeof module !== 'undefined' && !module.nodeType && module.exports) {
            exports = module.exports = CNPJ;
        }

        exports.CNPJ = CNPJ;
    } else {
        root.CNPJ = CNPJ;
    }

    CNPJ.validate = function (c) {
        var b = [6,5,4,3,2,9,8,7,6,5,4,3,2];

        if((c = c.replace(/[^\d]/g,"")).length != 14)
            return false;

        if(/0{14}/.test(c))
            return false;

        for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]);
        if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
            return false;

        for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]);
        if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
            return false;

        return true;

    };

    return CNPJ;
})();