$(document).ready(function() {
    $('#date-mask-input').mask("00/00/0000", {placeholder: "__/__/____"});
    $('#time-mask-input').mask('00:00:00', {placeholder: "__:__:__"});
    $('#date-and-time-mask-input').mask('00/00/0000 00:00:00', {placeholder: "__/__/____ __:__:__"});
    $('#cep-mask').mask('00000-000', {placeholder: "_____-___"});
    $('#money-mask-input').mask('000.000.000.000.000,00', {reverse: true});
    $('#phone-mask-input').mask('0000-0000', {placeholder: "____-____"});
    $('#phone-with-code-area-mask-input').mask('(00) 0000-0000', {placeholder: "(__) ____-____"});
    $('#us-phone-mask-input').mask('(000) 000-0000', {placeholder: "(___) ___-____"});
    $('#ip-address-mask-input').mask('099.099.099.099');
    $('#mixed-mask-input').mask('AAA 000-S0S');
    $('#placa-mask').mask('AAA0000');
    $('#volume-mask').mask('000.000.000.000.000', {reverse: true});
    $('.volume-mask').mask('000.000.000.000.000', {reverse: true});
    $('#cpf-mask').mask('000.000.000-00', {reverse: true});
    $('#cnpj-mask').mask('00.000.000/0000-00');
    $('#cnh-mask').mask('00000000000');
    $('#renavam-mask').mask('00000000000');
    $('#exp-mask').mask('000000/00');
    $('#filhote-mask').mask('000');

    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };
    $('.sp_phones').mask(SPMaskBehavior, spOptions);

});
